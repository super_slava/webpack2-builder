import '../scss/main.scss';
import HomePage from 'app/HomePage';

window.addEventListener('load', ()=> {
	
	/** @type {MainPage} */
	window.home = new HomePage();

} );